<?php 
	include('functions.php');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Registration</title>
    <link rel="stylesheet" type="text/css" href="main.css">
</head>
<body>
<div class="header">
		<h2>Student Registration</h2>
	</div>
<div class="header">
	<h4>Register</h4>
</div>
<form method="post" action="student_register.php">

	<div class="input-group">
		<label>Studentname</label>
		<input type="text" name="studentname" required>
	</div>
    <div class="input-group">
		<label>Studentregno.</label>
		<input type="text" name="studentregno" required>
	</div>
	<div class="input-group">
		<label>Email</label>
		<input type="email" name="email" required>
	</div>
    <div class="input-group">
		<label>Course</label>
		<input type="text" name="course" required >
	</div>
	<div class="input-group">
		<label>Password</label>
		<input type="password" name="password_1" required>
	</div>
	<div class="input-group">
		<label>Confirm password</label>
		<input type="password" name="password_2" required>
	</div>
	<div class="input-group">
		<button type="submit" class="btn" name="register">Register</button>
	</div>
	<p>
		Already registered? <a href="studentlogin.php">Sign in</a>
	</p>
</form>
</body>
</html>
<?php

//call the register() function if register_btn is clicked
$db = mysqli_connect('localhost', 'root', '', 'erms');
if (isset($_POST['register'])) {
	  // defined below to escape form values
	$studentname    =   $_POST['studentname'];
	$studentregno    =  $_POST['studentregno'];
	$email       =      $_POST['email'];
	$course       =     $_POST['course'];
	$password_1  =      $_POST['password_1'];
	$password_2  =      $_POST['password_2'];
	$errors          = array(); 


	// form validation: ensure that the form is correctly filled
	if (empty($studentname)) { 
		array_push($errors, "studentname is required"); 
	}
	if (empty($studentregno)) { 
		array_push($errors, "studentregno is required"); 
	}
	if (empty($email)) { 
		array_push($errors, "Email is required"); 
	}
	if (empty($course)) { 
		array_push($errors, "course is required"); 
	}
	if (empty($password_1)) { 
		array_push($errors, "Password is required"); 
	}
	if ($password_1 != $password_2) {
		array_push($errors, "The two passwords do not match");
	}

	// register user if there are no errors in the form
	if (count($errors) == 0) {
		$password = md5($password_1);//encrypt the password before saving in the database

			$query = "INSERT INTO student (studentname, studentregno, course, email, password) 
					  VALUES('$studentname','$studentregno', '$course','$email', '$password')";
		    $run_query = mysqli_query($db,$query);
			if($run_query){

				echo "<script>alert('student has been inserted successfully')</script>";
				echo "<script>window.open('student.php')</script>";
			} else {
				echo "<script>alert('Insert all fields')</script>";

			}
		}
		}
?>