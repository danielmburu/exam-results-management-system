<?php 
include('functions.php');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Home</title>
	<link rel="stylesheet" type="text/css" href="main.css">
	<link rel="stylesheet" href="css/bootstrap-337.min.css">
</head>
<body>
	<div class="header">
		<h2>Home Page</h2>
	</div>
	<div class="content">
	<div class="content">

<!-- logged in user information -->
<div class="profile_info">
	<img src="../images\user_profile.png"  >
	<div>
		<br>
	<a href="logout1.php" style="color: red;">logout</a>
			</small>	
	</div>
			</div>
		</div>
	</div>
	<div class="row"><!--row begin -->
  <div class="col-lg-12"><!--col-lg-12 begin -->
    <div class="breadcrumb"><!--breadcrumb begin -->
      <li class="active"><!--active begin -->
        <i class="fa fa-dashboard"></i>Dashboard / view students
      </li><!--active finish -->
    </div><!--breadcrumb finish -->
  </div><!--col-lg-12 finish -->
</div><!--row finish -->
<div class="input-group">
			<button type="submit" style="padding: 15px;
			                             margin-left: 1300px;
										  margin: 0px 0px 0px 1300px;
	                                     font-size: 15px;
	                                     color: white;
	                                     background: #c2a23a;
	                                     border: none;
	                                     border-radius: 5px;" name="login"><a href="addresults.php">Add new results</a></button>
		</div>
       <div class="panel-body"><!--panel-body begin 2 -->
           <div class="table-responsive"><!--table-responsive begin 2 -->
               <table class="table table-striped table-bordered table-hover"><!--table-striped finish -->
                 <thead><!--thead begin 2 -->
                  <tr><!--tr begin 2 -->
                    <td>Registration number</td>
                    <td>Semester</td>
                    <td>Unit</td>
                    <td>Marks</td>
                    <td>Grade</td>
                    <td>Remarks</td>
                    <td>Delete</td>
					<td>Edit</td>
                  </tr><!--tr finish -->
                 </thead><!--thead finish -->
                   <tbody>
                     <?php
                      $i=0;
                      $get_results = "select * from exam_results";
                      $run_results = mysqli_query($db, $get_results);

                      while($row_results = mysqli_fetch_array($run_results)){
                          $regno = $row_results['studentregno'];
                          $semester = $row_results['semester'];
                          $unit = $row_results['unit_title'];
                          $marks = $row_results['score'];
                          $grade = $row_results['grade'];
                          $remarks  = $row_results['remarks'];
                         $i++;
                      
                     ?>
                     <tr><!--tr begin -->
                         <td><?php echo $regno ?></td>
                         <td><?php echo $semester; ?></td>
                         <td><?php echo $unit; ?></td>
                         <td><?php echo $marks; ?></td>
                         <td><?php echo $grade; ?></td>
                         <td><?php echo $remarks; ?></td>
						 <td>
                          <a href="index.php?delete_customer=<?php echo $customer_id ?>">
                            <i class="fa fa-trash-o"></i>Delete
                          </a>
                         </td>
						 <td>
                          <a href="index.php?delete_customer=<?php echo $customer_id ?>">
                            <i class="fa fa-trash-o"></i>edit
                          </a>
                         </td>
                     </tr><!--tr finish -->
                     <?php } ?>
                   </tbody>
               </table><!--panel-body begin 2 -->
           </div><!--table-responsive finish -->
       </div><!--table-striped finish -->
    </div><!--panel panel-defaultfinish -->
  </div><!--col-lg-12 finish -->
</div><!--row finish -->
</body>
</html>

