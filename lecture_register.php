<?php 
	include('functions.php');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Registration</title>
    <link rel="stylesheet" type="text/css" href="main.css">
</head>
<body>
<div class="header">
		<h2>Lecture Registration</h2>
	</div>
<div class="header">
	<h4>Register</h4>
</div>
<form method="post" action="lecture_register.php">
	<div class="input-group">
		<label>Staffname</label>
		<input type="text" name="staffname" value="">
	</div>
    <div class="input-group">
		<label>Staffno.</label>
		<input type="text" name="staffno" value="">
	</div>
	<div class="input-group">
		<label>Email</label>
		<input type="email" name="email" value="">
	</div>
    <div class="input-group">
		<label>Department</label>
		<input type="text" name="department" value="">
	</div>
	<div class="input-group">
		<label>Password</label>
		<input type="password" name="password_1">
	</div>
	<div class="input-group">
		<label>Confirm password</label>
		<input type="password" name="password_2">
	</div>
	<div class="input-group">
		<button type="submit" class="btn" name="register">Register</button>
	</div>
	<p>
		Already registered? <a href="lecture_login.php">Sign in</a>
	</p>
</form>
</body>
</html>
<?php

//call the register() function if register_btn is clicked
$db = mysqli_connect('localhost', 'root', '', 'erms');
if (isset($_POST['register'])) {
	  // defined below to escape form values
	$staffname    =   $_POST['staffname'];
	$staffno    =  $_POST['staffno'];
	$email       =      $_POST['email'];
	$department       =     $_POST['department'];
	$password_1  =      $_POST['password_1'];
	$password_2  =      $_POST['password_2'];
	$errors          = array(); 


	// form validation: ensure that the form is correctly filled
	if (empty($staffname)) { 
		array_push($errors, "staffname is required"); 
	}
	if (empty($staffno)) { 
		array_push($errors, "staffno. is required"); 
	}
	if (empty($email)) { 
		array_push($errors, "Email is required"); 
	}
	if (empty($department)) { 
		array_push($errors, "department is required"); 
	}
	if (empty($password_1)) { 
		array_push($errors, "Password is required"); 
	}
	if ($password_1 != $password_2) {
		array_push($errors, "The two passwords do not match");
	}

	// register user if there are no errors in the form
	if (count($errors) == 0) {
		$password = md5($password_1);//encrypt the password before saving in the database

			$query = "INSERT INTO lecture (staffname, staffno,email, department, password) 
					  VALUES('$staffname','$staffno','$email', '$department', '$password')";
		    $run_query = mysqli_query($db,$query);
			if($run_query){

				echo "<script>alert('lecture has been inserted successfully')</script>";
				echo "<script>window.open('lecture.php')</script>";
			} else {
				echo "<script>alert('Insert all fields')</script>";

			}
		}
		}
?>
