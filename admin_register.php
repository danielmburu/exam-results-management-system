<!DOCTYPE html>
<html>
<head>
	<title>Registration</title>
    <link rel="stylesheet" type="text/css" href="main.css">
</head>
<body>
<div class="header">
		<h2>Student Registration</h2>
	</div>
<div class="header">
	<h4>Register</h4>
</div>
<form method="post" action="register.php">
	<div class="input-group">
		<label>admin name</label>
		<input type="text" name="admin_name" value="">
	</div>
    <div class="input-group">
		<label>Admin id</label>
		<input type="text" name="admin_id" value="">
	</div>
	<div class="input-group">
		<label>Email</label>
		<input type="email" name="email" value="">
	</div>
   
	<div class="input-group">
		<label>Password</label>
		<input type="password" name="password_1">
	</div>
	<div class="input-group">
		<label>Confirm password</label>
		<input type="password" name="password_2">
	</div>
	<div class="input-group">
		<button type="submit" class="btn" name="register_btn">Register</button>
	</div>
	<p>
		Already registered? <a href="admin_login.php">Sign in</a>
	</p>
</form>
</body>
</html>
