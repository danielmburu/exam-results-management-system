<!DOCTYPE html>
<html>
<head>
	<title>Registration</title>
    <link rel="stylesheet" type="text/css" href="main.css">
</head>
<body>
<div class="header">
		<h2>Admin Login</h2>
	</div>
<div class="header">
	<h4>Login</h4>
</div>
<form method="post" action="dashboard.php">

		<div class="input-group">
			<label>Email</label>
			<input type="text" name="username" >
		</div>
		<div class="input-group">
			<label>Password</label>
			<input type="password" name="password">
		</div>
		<div class="input-group">
			<button type="submit" class="btn" name="login_btn">Login</button>
		</div>
		<p>
			Not yet registered? <a href="admin_register.php">Sign up</a>
		</p>
	</form>
</body>
</html>
