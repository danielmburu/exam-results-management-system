<?php 
include('functions.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> Insert results </title>
    <link rel="stylesheet" href="css/bootstrap-337.min.css">
    <link rel="stylesheet" type="text/css" href="main.css">
</head>
<body>
    
<div class="row"><!-- row Begin -->
    
    <div class="col-lg-12"><!-- col-lg-12 Begin -->
        
        <ol class="breadcrumb"><!-- breadcrumb Begin -->
            
            <li class="active"><!-- breadcrumb Begin -->
                
                <i class="fa fa-dashboard"></i> Dashboard / Insert results
                
            </li><!-- breadcrumb Finish -->
            
        </ol><!-- breadcrumb Finish -->
        
    </div><!-- col-lg-12 Finish -->
    
</div><!-- row Finish -->
       
<div class="row"><!-- row Begin -->
    
    <div class="col-lg-12"><!-- col-lg-12 Begin -->
        
        <div class="panel panel-default"><!-- panel panel-default Begin -->
            
           <div class="panel-heading"><!-- panel panel-default Begin -->
               
               <h3 class="panel-title"><!-- panel-title Begin -->
                   
                   <i class="fa fa-money fa-fw"></i> Insert results
                   
               </h3><!-- panel-title Finish -->
               
           </div> <!-- canel panel-default Finish -->
           
           <div class="panel-body"><!-- panel-body Begin -->
               
           <form method="post" action="addresults.php">
	<div class="input-group">
		<label>studentregno.</label>
		<input type="text" name="studentregno" value="">
	</div>
    <div class="input-group">
		<label>unit_title</label>
		<input type="text" name="unit_title" value="">
	</div>
	<div class="input-group">
		<label>unit_code</label>
		<input type="text" name="unit_code" value="">
	</div>
    <div class="input-group">
		<label>semester</label>
		<input type="text" name="semester" value="">
	</div>
    <div class="input-group">
		<label>score</label>
		<input type="text" name="score" value="">
	</div>
	<div class="input-group">
		<label>grade</label>
		<input type="text" name="grade">
	</div>
	<div class="input-group">
		<label>remarks</label>
        <textarea name="remarks" cols="19" rows="6" class="form-control"></textarea>
	</div>
	<div class="input-group">
		<button type="submit" class="btn" name="submit">Add</button>
	</div>
</form>
               
           </div><!-- panel-body Finish -->
            
        </div><!-- canel panel-default Finish -->
        
    </div><!-- col-lg-12 Finish -->
    
</div><!-- row Finish -->
</body>
</html>
<!--inserting products to the database-->
<?php 
  
  if(isset($_POST['submit'])) {

    $studentregno = $_POST['studentregno'];
    $unit_title = $_POST['unit_title'];
    $unit_code = $_POST['unit_code'];
    $semester = $_POST['semester'];
    $score = $_POST['score'];
    $grade = $_POST['grade'];
    $remarks = $_POST['remarks'];


    $add_results = "insert into exam_results
    (studentregno,unit_title,unit_code,semester,score,grade,remarks) values
    ('$studentregno','$unit_title','$unit_code','$semester','$score','$grade','$remarks')";

    $run_results = mysqli_query($db,$add_results);

    if($run_results){

        echo "<script>alert('results has been inserted successfully')</script>";
        echo "<script>window.open('lecture.php','_self')</script>";
    }
  }
?>
